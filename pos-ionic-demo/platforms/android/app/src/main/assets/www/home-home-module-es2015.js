(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["home-home-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.page.html":
/*!***************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.page.html ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header [translucent]=\"true\">\r\n  <ion-toolbar>\r\n    <ion-title>\r\n      MPOS Demo\r\n    </ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content [fullscreen]=\"true\">\r\n  <ion-header collapse=\"condense\">\r\n    <!-- <link rel=\"stylesheet\" href=\"home.page.scss\"> -->\r\n    <ion-toolbar>\r\n      <ion-title size=\"large\">MPOS Demo</ion-title>\r\n    </ion-toolbar>\r\n  </ion-header>\r\n\r\n  <div>\r\n    <button (click)=\"scanQPos2Mode()\" class=\"ButtonStyle\">scan</button>\r\n    <button (click)=\"dotrade()\" class=\"ButtonStyle\">start trade</button>\r\n    <button (click)=\"disConnectBT()\" class=\"ButtonStyle\">disconnect</button>\r\n    <button (click)=\"updateEMVConfigureByXml()\" class=\"ButtonStyle\">updateEMVConfigure</button>\r\n  </div>\r\n  \r\n  <div id=\"tablediv\" class=\"layui-table-body layui-table-main\" style=\"height:300px;margin-top:10px;\" >\r\n        <table class=\"layui-table\" lay-skin=\"line\" style=\"overflow:auto; margin-top:10px; width:90%; margin-left: 5%; margin-right: 5%; text-align:left; border-collapse:separate; border-spacing:0px 10px;\" id=\"bluTable\">\r\n            <colgroup>\r\n                <col width=\"200\">\r\n                <col width=\"150\">\r\n                <col width=\"200\">\r\n                <col>\r\n            </colgroup>\r\n            <thead>\r\n            <tr>\r\n                <th>Bluetooth Name</th>\r\n            </tr>\r\n            </thead>\r\n            <tbody>\r\n            </tbody>\r\n        </table>\r\n</div>\r\n\r\n<textarea id=\"posResult\" style=\"overflow:scroll; overflow-x:hidden; height:350px;width:90%; margin-top: 10px; margin-left: 5%; margin-right: 5%; margin-bottom: 20px; display: none;\" readonly></textarea>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/home/home-routing.module.ts":
/*!*********************************************!*\
  !*** ./src/app/home/home-routing.module.ts ***!
  \*********************************************/
/*! exports provided: HomePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageRoutingModule", function() { return HomePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./home.page */ "./src/app/home/home.page.ts");




const routes = [
    {
        path: '',
        component: _home_page__WEBPACK_IMPORTED_MODULE_3__["HomePage"],
    }
];
let HomePageRoutingModule = class HomePageRoutingModule {
};
HomePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], HomePageRoutingModule);



/***/ }),

/***/ "./src/app/home/home.module.ts":
/*!*************************************!*\
  !*** ./src/app/home/home.module.ts ***!
  \*************************************/
/*! exports provided: HomePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageModule", function() { return HomePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./home.page */ "./src/app/home/home.page.ts");
/* harmony import */ var _home_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home-routing.module */ "./src/app/home/home-routing.module.ts");







let HomePageModule = class HomePageModule {
};
HomePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonicModule"],
            _home_routing_module__WEBPACK_IMPORTED_MODULE_6__["HomePageRoutingModule"]
        ],
        declarations: [_home_page__WEBPACK_IMPORTED_MODULE_5__["HomePage"]]
    })
], HomePageModule);



/***/ }),

/***/ "./src/app/home/home.page.scss":
/*!*************************************!*\
  !*** ./src/app/home/home.page.scss ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("#container {\n  text-align: center;\n  position: absolute;\n  left: 0;\n  right: 0;\n  top: 50%;\n  transform: translateY(-50%);\n}\n\n#container strong {\n  font-size: 20px;\n  line-height: 26px;\n}\n\n#container p {\n  font-size: 16px;\n  line-height: 22px;\n  color: #8c8c8c;\n  margin: 0;\n}\n\n#container a {\n  text-decoration: none;\n}\n\n.ButtonStyle {\n  margin-top: 10px;\n  height: 40px;\n  color: white;\n  background-color: #4CAF50;\n  /* Green */\n  border-radius: 5px;\n  width: 90%;\n  margin-top: 10px;\n  margin-left: 5%;\n  margin-right: 5%;\n}\n\n.ButtonStyle:active {\n  margin-top: 10px;\n  height: 40px;\n  color: white;\n  border-radius: 5px;\n  background: #000000;\n  opacity: 0.5;\n  width: 90%;\n  margin-top: 10px;\n  margin-left: 5%;\n  margin-right: 5%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaG9tZS9ob21lLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGtCQUFBO0VBRUEsa0JBQUE7RUFDQSxPQUFBO0VBQ0EsUUFBQTtFQUNBLFFBQUE7RUFDQSwyQkFBQTtBQUFGOztBQUdBO0VBQ0UsZUFBQTtFQUNBLGlCQUFBO0FBQUY7O0FBR0E7RUFDRSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxjQUFBO0VBQ0EsU0FBQTtBQUFGOztBQUdBO0VBQ0UscUJBQUE7QUFBRjs7QUFHQTtFQUNFLGdCQUFBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7RUFDQSx5QkFBQTtFQUEyQixVQUFBO0VBQzNCLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0FBQ0Y7O0FBRUE7RUFDRSxnQkFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtFQUNBLFlBQUE7RUFDQSxVQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7QUFDRiIsImZpbGUiOiJzcmMvYXBwL2hvbWUvaG9tZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIjY29udGFpbmVyIHtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcblxyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICBsZWZ0OiAwO1xyXG4gIHJpZ2h0OiAwO1xyXG4gIHRvcDogNTAlO1xyXG4gIHRyYW5zZm9ybTogdHJhbnNsYXRlWSgtNTAlKTtcclxufVxyXG5cclxuI2NvbnRhaW5lciBzdHJvbmcge1xyXG4gIGZvbnQtc2l6ZTogMjBweDtcclxuICBsaW5lLWhlaWdodDogMjZweDtcclxufVxyXG5cclxuI2NvbnRhaW5lciBwIHtcclxuICBmb250LXNpemU6IDE2cHg7XHJcbiAgbGluZS1oZWlnaHQ6IDIycHg7XHJcbiAgY29sb3I6ICM4YzhjOGM7XHJcbiAgbWFyZ2luOiAwO1xyXG59XHJcblxyXG4jY29udGFpbmVyIGEge1xyXG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxufVxyXG5cclxuLkJ1dHRvblN0eWxle1xyXG4gIG1hcmdpbi10b3A6IDEwcHg7XHJcbiAgaGVpZ2h0OiA0MHB4O1xyXG4gIGNvbG9yOiB3aGl0ZTtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjNENBRjUwOyAvKiBHcmVlbiAqL1xyXG4gIGJvcmRlci1yYWRpdXM6IDVweDtcclxuICB3aWR0aDogOTAlO1xyXG4gIG1hcmdpbi10b3A6IDEwcHg7XHJcbiAgbWFyZ2luLWxlZnQ6IDUlO1xyXG4gIG1hcmdpbi1yaWdodDogNSU7XHJcbn1cclxuXHJcbi5CdXR0b25TdHlsZTphY3RpdmV7XHJcbiAgbWFyZ2luLXRvcDogMTBweDtcclxuICBoZWlnaHQ6IDQwcHg7XHJcbiAgY29sb3I6IHdoaXRlO1xyXG4gIGJvcmRlci1yYWRpdXM6IDVweDtcclxuICBiYWNrZ3JvdW5kOiAjMDAwMDAwOyBcclxuICBvcGFjaXR5IDogMC41OyAgICAvL+i/memHjOmHjeimge+8jOWwseaYr+mAmui/h+i/meS4qumAj+aYjuW6puadpeiuvue9rlxyXG4gIHdpZHRoOiA5MCU7XHJcbiAgbWFyZ2luLXRvcDogMTBweDtcclxuICBtYXJnaW4tbGVmdDogNSU7XHJcbiAgbWFyZ2luLXJpZ2h0OiA1JTtcclxufSJdfQ== */");

/***/ }),

/***/ "./src/app/home/home.page.ts":
/*!***********************************!*\
  !*** ./src/app/home/home.page.ts ***!
  \***********************************/
/*! exports provided: HomePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePage", function() { return HomePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


/**
    * Transaction type.
    */
var TransactionType;
(function (TransactionType) {
    TransactionType[TransactionType["GOODS"] = 0] = "GOODS";
    TransactionType[TransactionType["SERVICES"] = 1] = "SERVICES";
    TransactionType[TransactionType["CASH"] = 2] = "CASH";
    TransactionType[TransactionType["CASHBACK"] = 3] = "CASHBACK";
    TransactionType[TransactionType["INQUIRY"] = 4] = "INQUIRY";
    TransactionType[TransactionType["TRANSFER"] = 5] = "TRANSFER";
    TransactionType[TransactionType["ADMIN"] = 6] = "ADMIN";
    TransactionType[TransactionType["CASHDEPOSIT"] = 7] = "CASHDEPOSIT";
    TransactionType[TransactionType["PAYMENT"] = 8] = "PAYMENT";
    TransactionType[TransactionType["PBOCLOG"] = 9] = "PBOCLOG";
    TransactionType[TransactionType["SALE"] = 10] = "SALE";
    TransactionType[TransactionType["PREAUTH"] = 11] = "PREAUTH";
    TransactionType[TransactionType["ECQ_DESIGNATED_LOAD"] = 12] = "ECQ_DESIGNATED_LOAD";
    TransactionType[TransactionType["ECQ_UNDESIGNATED_LOAD"] = 13] = "ECQ_UNDESIGNATED_LOAD";
    TransactionType[TransactionType["ECQ_CASH_LOAD"] = 14] = "ECQ_CASH_LOAD";
    TransactionType[TransactionType["ECQ_CASH_LOAD_VOID"] = 15] = "ECQ_CASH_LOAD_VOID";
    TransactionType[TransactionType["ECQ_INQUIRE_LOG"] = 16] = "ECQ_INQUIRE_LOG";
    TransactionType[TransactionType["REFUND"] = 17] = "REFUND";
    TransactionType[TransactionType["UPDATE_PIN"] = 18] = "UPDATE_PIN";
    TransactionType[TransactionType["SALES_NEW"] = 19] = "SALES_NEW";
    TransactionType[TransactionType["NON_LEGACY_MONEY_ADD"] = 20] = "NON_LEGACY_MONEY_ADD";
    TransactionType[TransactionType["LEGACY_MONEY_ADD"] = 21] = "LEGACY_MONEY_ADD"; /*0x16*/
})(TransactionType || (TransactionType = {}));
var CardTradeMode;
(function (CardTradeMode) {
    CardTradeMode[CardTradeMode["ONLY_INSERT_CARD"] = 0] = "ONLY_INSERT_CARD";
    CardTradeMode[CardTradeMode["ONLY_SWIPE_CARD"] = 1] = "ONLY_SWIPE_CARD";
    CardTradeMode[CardTradeMode["TAP_INSERT_CARD"] = 2] = "TAP_INSERT_CARD";
    CardTradeMode[CardTradeMode["TAP_INSERT_CARD_NOTUP"] = 3] = "TAP_INSERT_CARD_NOTUP";
    CardTradeMode[CardTradeMode["SWIPE_TAP_INSERT_CARD"] = 4] = "SWIPE_TAP_INSERT_CARD";
    CardTradeMode[CardTradeMode["UNALLOWED_LOW_TRADE"] = 5] = "UNALLOWED_LOW_TRADE";
    CardTradeMode[CardTradeMode["SWIPE_INSERT_CARD"] = 6] = "SWIPE_INSERT_CARD";
    CardTradeMode[CardTradeMode["SWIPE_TAP_INSERT_CARD_UNALLOWED_LOW_TRADE"] = 7] = "SWIPE_TAP_INSERT_CARD_UNALLOWED_LOW_TRADE";
    CardTradeMode[CardTradeMode["SWIPE_TAP_INSERT_CARD_NOTUP_UNALLOWED_LOW_TRADE"] = 8] = "SWIPE_TAP_INSERT_CARD_NOTUP_UNALLOWED_LOW_TRADE";
    CardTradeMode[CardTradeMode["ONLY_TAP_CARD"] = 9] = "ONLY_TAP_CARD";
    CardTradeMode[CardTradeMode["ONLY_TAP_CARD_QF"] = 10] = "ONLY_TAP_CARD_QF";
    CardTradeMode[CardTradeMode["SWIPE_TAP_INSERT_CARD_NOTUP"] = 11] = "SWIPE_TAP_INSERT_CARD_NOTUP";
    CardTradeMode[CardTradeMode["SWIPE_TAP_INSERT_CARD_DOWN"] = 12] = "SWIPE_TAP_INSERT_CARD_DOWN";
    CardTradeMode[CardTradeMode["SWIPE_INSERT_CARD_UNALLOWED_LOW_TRADE"] = 13] = "SWIPE_INSERT_CARD_UNALLOWED_LOW_TRADE";
    CardTradeMode[CardTradeMode["SWIPE_TAP_INSERT_CARD_UNALLOWED_LOW_TRADE_NEW"] = 14] = "SWIPE_TAP_INSERT_CARD_UNALLOWED_LOW_TRADE_NEW";
    CardTradeMode[CardTradeMode["ONLY_INSERT_CARD_NOPIN"] = 15] = "ONLY_INSERT_CARD_NOPIN";
})(CardTradeMode || (CardTradeMode = {}));
/**
    * The different check value for different key type.
    */
var CHECKVALUE_KEYTYPE;
(function (CHECKVALUE_KEYTYPE) {
    /**
     * TMK of MKSK
     */
    CHECKVALUE_KEYTYPE[CHECKVALUE_KEYTYPE["MKSK_TMK"] = 0] = "MKSK_TMK";
    /**
     * PIK of MKSK
     */
    CHECKVALUE_KEYTYPE[CHECKVALUE_KEYTYPE["MKSK_PIK"] = 1] = "MKSK_PIK";
    /**
     * TDK of MKSK
     */
    CHECKVALUE_KEYTYPE[CHECKVALUE_KEYTYPE["MKSK_TDK"] = 2] = "MKSK_TDK";
    /**
     * MCK of MKSK
     */
    CHECKVALUE_KEYTYPE[CHECKVALUE_KEYTYPE["MKSK_MCK"] = 3] = "MKSK_MCK";
    /**
     * TCK
     */
    CHECKVALUE_KEYTYPE[CHECKVALUE_KEYTYPE["TCK"] = 4] = "TCK";
    /**
     * MAGK
     */
    CHECKVALUE_KEYTYPE[CHECKVALUE_KEYTYPE["MAGK"] = 5] = "MAGK";
    /**
     * TRK TPEK of DUKPT
     */
    CHECKVALUE_KEYTYPE[CHECKVALUE_KEYTYPE["DUKPT_TRK_IPEK"] = 6] = "DUKPT_TRK_IPEK";
    /**
     * EMV IPEK of DUKPT
     */
    CHECKVALUE_KEYTYPE[CHECKVALUE_KEYTYPE["DUKPT_EMV_IPEK"] = 7] = "DUKPT_EMV_IPEK";
    /**
     * PIN IPEK of DUKPT
     */
    CHECKVALUE_KEYTYPE[CHECKVALUE_KEYTYPE["DUKPT_PIN_IPEK"] = 8] = "DUKPT_PIN_IPEK";
    /**
     * TRK KSN of DUKPT
     */
    CHECKVALUE_KEYTYPE[CHECKVALUE_KEYTYPE["DUKPT_TRK_KSN"] = 9] = "DUKPT_TRK_KSN";
    /**
     * EMV KSN of DUKPT
     */
    CHECKVALUE_KEYTYPE[CHECKVALUE_KEYTYPE["DUKPT_EMV_KSN"] = 10] = "DUKPT_EMV_KSN";
    /**
     * PIN KSN of DUKPT
     */
    CHECKVALUE_KEYTYPE[CHECKVALUE_KEYTYPE["DUKPT_PIN_KSN"] = 11] = "DUKPT_PIN_KSN";
    /**
     * ALL TYPE of DUKPT and MKSK
     */
    CHECKVALUE_KEYTYPE[CHECKVALUE_KEYTYPE["DUKPT_MKSK_ALLTYPE"] = 12] = "DUKPT_MKSK_ALLTYPE";
})(CHECKVALUE_KEYTYPE || (CHECKVALUE_KEYTYPE = {}));
let HomePage = class HomePage {
    constructor() {
        console.log("init....");
    }
    scanQPos2Mode() {
        console.log("scanQPos2Mode");
        var tabled = document.getElementById("tablediv");
        var txtresult = document.getElementById("posResult");
        tabled.style.display = 'block';
        txtresult.style.display = 'none';
        cordova.plugins.dspread_pos_plugin.scanQPos2Mode((success) => {
            console.log("scanQPos2Mode->success: " + success);
            this.addTorow(success);
        }, (fail) => {
            console.log("scanQPos2Mode->fail: " + fail);
            this.posresult(fail);
        }, 30);
    }
    dotrade() {
        console.log("dotrade");
        // cordova.plugins.dspread_pos_plugin.setCardTradeMode((success : any) =>{
        //   console.log("setCardTradeMode->success: " + success);
        // },(fail : any) =>{
        //   console.log("setCardTradeMode->fail: " + fail);
        // },CardTradeMode.ONLY_SWIPE_CARD);
        cordova.plugins.dspread_pos_plugin.doTrade((success) => {
            console.log("dotrade->success: " + success);
            this.posresult(success);
            if (success == "onRequestSetAmount") {
                this.inputAmount();
            }
            else if (success.indexOf("onRequestOnlineProcess") == 0) {
                this.sendOnlineResult("8A023030");
            }
        }, (fail) => {
            console.log("dotrade->fail: " + fail);
            this.posresult(fail);
        }, 0, 20);
    }
    sendOnlineResult(tlv) {
        console.log("sendOnlineResult");
        cordova.plugins.dspread_pos_plugin.sendOnlineProcessResult((success) => {
            console.log("sendOnlineProcessResult->success: " + success);
        }, (fail) => {
            console.log("sendOnlineProcessResult->fail: " + fail);
        }, tlv);
    }
    getKeyCheckValue(keyIndex, keyType) {
        console.log("getKeyCheckValue");
        cordova.plugins.dspread_pos_plugin.getKeyCheckValue((success) => {
            console.log("getKeyCheckValue->success: " + success);
        }, (fail) => {
            console.log("getKeyCheckValue->fail: " + fail);
        }, keyIndex, keyType);
    }
    disConnectBT() {
        console.log("disConnectBT");
        cordova.plugins.dspread_pos_plugin.disconnectBT((success) => {
            console.log("disConnectBT->success: " + success);
            this.posresult(success);
        }, (fail) => {
            console.log("disConnectBT->fail: " + fail);
            this.posresult(fail);
        });
    }
    updateEMVConfigureByXml() {
        this.posresult("start update emv configure, pls wait...");
        var rawFile = new XMLHttpRequest();
        var readResult = false;
        var allText;
        rawFile.open("GET", 'emv_profile_tlv.xml', false);
        rawFile.onreadystatechange = function () {
            if (rawFile.readyState === 4) {
                if (rawFile.status === 200 || rawFile.status == 0) {
                    allText = rawFile.responseText;
                    console.log("success:allText " + allText);
                    readResult = true;
                }
            }
        };
        rawFile.send(null);
        if (readResult) {
            cordova.plugins.dspread_pos_plugin.updateEMVConfigByXml((success) => {
                this.posresult(success);
                console.log("success: " + success);
            }, (fail) => {
                console.log("fail: " + fail);
                this.posresult(fail);
            }, allText);
        }
    }
    addTorow(str) {
        var tbody = document.querySelector('tbody');
        var tr = document.createElement('tr');
        var td1 = document.createElement('td');
        td1.innerHTML = str.split(' ')[0];
        tr.append(td1);
        tbody.append(tr);
        tr.onclick = () => {
            console.log("click " + str);
            this.posresult("bluetooth connecting...");
            cordova.plugins.dspread_pos_plugin.connectBluetoothDevice((success) => {
                console.log("connectBluetoothDevice->success" + success);
                this.posresult(success);
            }, (fail) => {
                console.log("connectBluetoothDevice->fail" + fail);
                this.posresult(fail);
            }, true, str);
        };
    }
    posresult(text) {
        var tbody = document.querySelector('tbody');
        var tabled = document.getElementById("tablediv");
        var txtresult = document.getElementById("posResult");
        txtresult.style.display = "none";
        tabled.style.display = 'none';
        txtresult.style.display = 'block';
        txtresult.value = text;
    }
    inputAmount() {
        console.log("inputAmount");
        var confirmtd = prompt("please input amount", "12");
        console.log("TDsuccess:" + confirmtd);
        if (confirmtd) {
            cordova.plugins.dspread_pos_plugin.setAmount((success) => {
                console.log("scanQPos2Mode->success: " + success);
                this.posresult(success);
            }, (fail) => {
                console.log("scanQPos2Mode->fail: " + fail);
                this.posresult(fail);
            }, confirmtd, 0, "0156", TransactionType.GOODS);
        }
    }
    onReturnCustomConfigResult(str) {
        this.posresult(str);
    }
};
HomePage.ctorParameters = () => [];
HomePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-home',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./home.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./home.page.scss */ "./src/app/home/home.page.scss")).default]
    })
], HomePage);



/***/ })

}]);
//# sourceMappingURL=home-home-module-es2015.js.map